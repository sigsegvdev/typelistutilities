## TypeListUtilities
It's a header-only template library which provides simple type list implementation, along with some useful algorithms and utilities.
The main goal was to turn variadic list of types into a single entity, which can be treated like an array of types and be manipulated in a convenient manner.
The library was designed mostly with simplicity in mind, so you may consider using it if you feel like it fits your needs well and large metaprogramming libraries like Boost.Hana are overkill for what you want.
Implementation employs C++17 fold expression in many cases, so compile times don't get drastically hurt, compared to classic solutions.
This library may also serve good purpose as learning material for those who wish to explore the topic.  
  
For contents and description, check the header files. Provided examples should give you idea how to use this library. No building is required, unless you want to checkout results in the examples.
  
This library requires **C++17** support. Tested with g++ 8.3 and clang++ 8.0
