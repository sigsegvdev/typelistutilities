#!/bin/bash

set -e
cd "$( cd "$(dirname "$0")" ; pwd -P )"
mkdir -p build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j
