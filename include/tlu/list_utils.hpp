#pragma once

#include <tlu/list.hpp>

namespace tlu {

/// Helper type transformation which gives type unchanged
template<typename T>
using identity = TypeHolder<T>;



/// Append Types at the end of ListT
template<typename ListT, typename ...Types>
struct append;

template<typename ...Ts, typename ...Types>
struct append<List<Ts...>, Types...> {
    using type = List<Ts..., Types...>;
};



/// Append Types at the end of ListT, without repetitions
/// Repeating types are omitted by comparing them to all
/// types in ListT for equality
/// Operation is a transformation applied on types before comparision
/// If you want for types to be compared as they are,
/// you can use append_distinct_i helper alias
template<template<typename> typename Operation,
         typename ListT, typename ...Types>
struct append_distinct {
    // Default case is recursion tail
    using type = ListT;
};

template<template<typename> typename Operation,
         typename T, typename ...Types, typename ...Ts>
struct append_distinct<Operation, List<Ts...>, T, Types...> 
: std::conditional_t<
    (std::is_same_v<typename Operation<Ts>::type,
                    typename Operation<T>::type> || ...),
    append_distinct<Operation, List<Ts...>, Types...>,
    append_distinct<Operation, List<Ts..., T>, Types...>
> {};

template<typename ListT, typename ...Types>
using append_distinct_i = append_distinct<identity, ListT, Types...>;



/// Make a list of distinct types out of given ListT
/// Operation is a transformation applied on types before comparision
template<template<typename> typename Operation, typename ListT>
struct make_distinct;

template<template<typename> typename Operation, typename ...Ts>
struct make_distinct<Operation, List<Ts...>> {
    using type = typename append_distinct<Operation, List<>, Ts...>::type;
};

template<typename ListT>
using make_distinct_i = make_distinct<identity, ListT>;



/// Make one list of types out of all types in given ListTs
template<typename...ListTs>
struct concatenate {
    // Default case handles empty ListTs...
    using type = List<>;
};

template<typename ...Ts1, typename ...Ts2, typename ...ListTs>
struct concatenate<List<Ts1...>, List<Ts2...>, ListTs...>
: concatenate<List<Ts1..., Ts2...>, ListTs...> {};

template<typename ...Ts1, typename ...Ts2>
struct concatenate<List<Ts1...>, List<Ts2...>> {
    using type = List<Ts1..., Ts2...>;
};

template<typename ListT>
struct concatenate<ListT> {
    using type = ListT;
};



/// Make one list of unique types out of all types in given ListTs
/// Operation is a transformation applied on types before comparision
template<template<typename> typename Operation, typename ...ListTs>
using merge = make_distinct<Operation, typename concatenate<ListTs...>::type>;

template<typename ...ListTs>
using merge_i = merge<identity, ListTs...>;



/// Apply given transformation Operation on all types in ListT
/// Get new list of types as a result
template<template<typename> typename Operation, typename ListT>
struct transform;

template<template<typename> typename Operation, typename ...Ts>
struct transform<Operation, List<Ts...>> {
    using type = List<typename Operation<Ts>::type...>;
};



/// Apply given Predicate on all types in ListT
/// Get new list out of types, for which Predicate evaluated to true
template<template<typename> typename Predicate, typename ListT>
struct filter {
    // Default case is recursion tail
    using type = ListT;
};

template<template<typename> typename Predicate, typename T, typename ...Ts>
struct filter<Predicate, List<T, Ts...>>
: std::conditional_t<
    Predicate<T>::value,
    concatenate<List<T>, typename filter<Predicate, List<Ts...>>::type>,
    filter<Predicate, List<Ts...>>
> {};



/// Get list of types from ListT in given range of indices,
/// from First to Last (excluding)
template<typename ListT, std::size_t First, std::size_t Last>
struct slice {
    // Default case for static assert and recursion tail
    static_assert(First <= ListT::size::value &&
                  Last <= ListT::size::value,
                  "Index out of range");
    using type = List<>;
};

template<typename T, typename ...Ts, std::size_t First, std::size_t Last>
struct slice<List<T, Ts...>, First, Last>
: slice<List<Ts...>, First - 1, Last - 1> {};

template<typename T, typename ...Ts, std::size_t Last>
struct slice<List<T, Ts...>, 0, Last> 
: concatenate<List<T>, typename slice<List<Ts...>, 0, Last - 1>::type> {};

template<typename T, typename ...Ts>
struct slice<List<T, Ts...>, 0, 0> {
    // T and Ts... required to prevent ambiguity
    using type = List<>;
};

} // namespace tlu
