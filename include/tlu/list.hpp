#pragma once

#include <tlu/utils.hpp>

namespace tlu {

/// Helper for storing type
template<typename T>
struct TypeHolder {
    using type = T;
};

/// Container for storing variadic list of types as template parameters
template<typename ...Ts>
struct List {
    /// Apply given function FN on all types in the List
    /// TypeHolder containing a type is passed as function argument on each call
    template<typename FN>
    constexpr static inline void for_each([[maybe_unused]] FN f) {
        (void)((f(TypeHolder<Ts>{}), ...));
    }

    /// Get number of types in List
    using size = std::integral_constant<std::size_t, sizeof...(Ts)>;

    /// Check whether all Types are present in the List
    template<typename ...Types>
    using contains = std::bool_constant<(tlu::contains<Types, Ts...>::value && ...)>;

    /// Get number of occurences of T in the List
    template<typename T>
    using count = tlu::count<T, Ts...>; 

    /// Get type at given index I in the List
    template<std::size_t I>
    using at = typename tlu::at<I, Ts...>::type;

    /// Get index of first occurence of T in the List
    template<typename T>
    using index = tlu::index<T, Ts...>;

    /// Check whether all types in the List are unique
    using is_distinct = tlu::is_distinct<Ts...>;

    /// Check if the List is a permutation of Types, that is, whether every
    /// type in the List has the same number of occurences as in Types
    template<typename ...Types>
    using is_permutation = std::bool_constant<
        (sizeof...(Types) == sizeof...(Ts)) && 
        ((count<Types>::value == tlu::count<Types, Types...>::value) && ...)
    >;
};

} // namespace tlu
