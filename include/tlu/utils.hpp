#pragma once

#include <cstddef>
#include <type_traits>

namespace tlu {

/// Get type at given index I in Ts
template<std::size_t I, typename ...Ts>
struct at {
    // Default case only for this assertion
    static_assert(I < sizeof...(Ts), "Type index out of range");
    // To suppress other errors when assertion fails
    using type = void;
};

template<std::size_t I, typename T, typename ...Ts>
struct at<I, T, Ts...> : at<I - 1, Ts...> {};

template<typename T, typename ...Ts>
struct at<0, T, Ts...> {
    using type = T;
};



/// Get index of first occurance of Type in Ts
template<typename Type, typename ...Ts>
struct index : std::integral_constant<std::size_t, 0> {
    // Default case only for this assertion
    static_assert(sizeof...(Ts) > 0, "Type is not in type list");
};

template<typename Type, typename T, typename ...Ts>
struct index<Type, T, Ts...>
: std::integral_constant<std::size_t, 1 + index<Type, Ts...>::value> {};

template<typename Type, typename ...Ts>
struct index<Type, Type, Ts...>
: std::integral_constant<std::size_t, 0> {};



/// Check whether Type occurs in Ts
template<typename Type, typename ...Ts>
struct contains : std::bool_constant<(std::is_same_v<Ts, Type> || ...)> {};



/// Get number of occurrences of Type in Ts
template<typename Type, typename ...Ts>
struct count : std::integral_constant<std::size_t, 0> {
    // Default case is recursion tail
};

template<typename Type, typename T, typename ...Ts>
struct count<Type, T, Ts...> : count<Type, Ts...> {};

template<typename Type, typename ...Ts>
struct count<Type, Type, Ts...>
: std::integral_constant<std::size_t, 1 + count<Type, Ts...>::value> {};



/// Check whether all types in Ts are unique
template<typename ...Ts>
struct is_distinct : std::true_type {
    // Default case handles 0 or 1 type in Ts...
};

template<typename T, typename ...Ts>
struct is_distinct<T, Ts...>
: std::bool_constant<!(std::is_same_v<T, Ts> || ...) &&
                     is_distinct<Ts...>::value> {};

} // namespace tlu
