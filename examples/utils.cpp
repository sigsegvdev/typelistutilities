#include "type_declarations.hpp"
#include <tlu/utils.hpp>
#include <iostream>

using namespace tlu;

int main(int, char**) {
    std::cout << "at:" << std::endl;
    std::cout << declared_name<typename at<3, ALL_DECLARED_TYPES_LIST>::type>::value << std::endl;
    std::cout << declared_name<typename at<7, ALL_DECLARED_TYPES_LIST>::type>::value << std::endl;
    std::cout << declared_name<typename at<5, ALL_DECLARED_TYPES_LIST>::type>::value << std::endl;

    std::cout << "index:" << std::endl;
    std::cout << index<unsigned, ALL_DECLARED_TYPES_LIST>::value << std::endl;
    std::cout << index<char, ALL_DECLARED_TYPES_LIST>::value << std::endl;
    std::cout << index<double, ALL_DECLARED_TYPES_LIST>::value << std::endl;

    std::cout << "contains:" << std::endl;
    std::cout << contains<long, ALL_DECLARED_TYPES_LIST>::value << std::endl;
    std::cout << contains<int>::value << std::endl;
    std::cout << contains<long double, ALL_DECLARED_TYPES_LIST>::value << std::endl;

    std::cout << "count:" << std::endl;
    std::cout << count<unsigned char, ALL_DECLARED_TYPES_LIST>::value << std::endl;
    std::cout << count<void*, ALL_DECLARED_TYPES_LIST>::value << std::endl;
    std::cout << count<char>::value << std::endl;
    std::cout << count<float, ALL_DECLARED_TYPES_LIST, float, float>::value << std::endl;

    std::cout << "is_distinct:" << std::endl;
    std::cout << is_distinct<ALL_DECLARED_TYPES_LIST>::value << std::endl;
    std::cout << is_distinct<ALL_DECLARED_TYPES_LIST, int, char>::value << std::endl;
    std::cout << is_distinct<>::value << std::endl;

    return 0;
}
