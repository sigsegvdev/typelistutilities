#include "type_declarations.hpp"
#include <tlu/list_utils.hpp>
#include <iostream>

template<int I>
using Int = std::integral_constant<int, I>;

template<int K, typename V>
struct MapItem {
    using key = Int<K>;
    using value = V;
};

using Map = tlu::List<
    MapItem<1, int>,
    MapItem<2, int>,
    MapItem<3, char>,
    MapItem<4, char>,
    MapItem<5, float>,
    MapItem<6, float>
>;

template<typename MapItemT>
struct MapItemKey {
    using type = typename MapItemT::key;
};

template<typename MapItemT>
struct MapItemValue {
    using type = typename MapItemT::value;
};

// Extract keys from map for lookup
using MapKeys = tlu::transform<MapItemKey, Map>::type;

// Lookup function
template<int I>
using GetValue = typename Map::at<(MapKeys::index<Int<I>>())>::value;

template<typename MapT>
void printMap() {
    MapT::for_each([](auto holder) {
        using MapItemT = typename decltype(holder)::type;
        std::cout << "k:" << MapItemT::key::value << '\t';
        std::cout << "v:" << declared_name<typename MapItemT::value>::value << std::endl;
    });
}

int main(int, char**) {
    std::cout << "Whole map:" << std::endl;
    printMap<Map>();
    // Access mapped types by their integer index
    std::cout << "Type lookup:" << std::endl;
    std::cout << declared_name<GetValue<6>>::value << std::endl;
    std::cout << declared_name<GetValue<2>>::value << std::endl;
    std::cout << declared_name<GetValue<3>>::value << std::endl;

    // Remove repeated mappings for the same value
    using DistinctValueMap = tlu::make_distinct<MapItemValue, Map>::type;
    std::cout << "No repeating values:" << std::endl;
    printMap<DistinctValueMap>();

    using OtherMap = tlu::List<
        MapItem<2, float>,
        MapItem<4, char>,
        MapItem<6, int>,
        MapItem<8, float>,
        MapItem<10, int>,
        MapItem<12, int>
    >;
    // Merge two maps and avoid repeating keys
    using MergedMap = tlu::merge<MapItemKey, Map, OtherMap>::type;
    std::cout << "Merged maps by key:" << std::endl;
    printMap<MergedMap>();

    return 0;
}