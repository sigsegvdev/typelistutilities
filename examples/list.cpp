#include "type_declarations.hpp"
#include <tlu/list.hpp>
#include <iostream>

using namespace tlu;

int main(int, char**) {
    std::cout << "for_each:" << std::endl;
    auto printTypeNames = [](auto holder) {
        using type = typename decltype(holder)::type;
        std::cout << declared_name<type>::value << std::endl;
    };
    List<int, char, double, int>::for_each(printTypeNames);
    List<>::for_each(printTypeNames);

    std::cout << "size:" << std::endl;
    std::cout << List<int, char, double>::size() << std::endl;
    std::cout << List<>::size() << std::endl;

    std::cout << "contains:" << std::endl;
    using ContainsList = List<int, char, double, int, long>;
    std::cout << ContainsList::contains<int, char>() << std::endl;
    std::cout << ContainsList::contains<int, unsigned char>() << std::endl;
    std::cout << List<>::contains<int>() << std::endl;
    std::cout << List<>::contains<>() << std::endl;

    std::cout << "is_distinct:" << std::endl;
    std::cout << List<int, unsigned int, char>::is_distinct() << std::endl;
    std::cout << List<int, char, int>::is_distinct() << std::endl;
    std::cout << List<int, int, int, int, int>::is_distinct() << std::endl;
    std::cout << List<>::is_distinct() << std::endl;

    std::cout << "at:" << std::endl;
    using AtList = List<int, unsigned, long, void, int, void>;
    std::cout << declared_name<AtList::at<0>>::value << std::endl;
    std::cout << declared_name<AtList::at<4>>::value << std::endl;
    std::cout << declared_name<AtList::at<2>>::value << std::endl;
    
    std::cout << "index:" << std::endl;
    using IndexList = List<int, unsigned, long, void, int, void>;
    std::cout << IndexList::index<int>() << std::endl;
    std::cout << IndexList::index<long>() << std::endl;
    std::cout << IndexList::index<void>() << std::endl;

    std::cout << "count:" << std::endl;
    using CountList = List<int, char, char, int, float, int>;
    std::cout << CountList::count<int>() << std::endl;
    std::cout << CountList::count<float>() << std::endl;
    std::cout << CountList::count<double>() << std::endl;
    std::cout << List<>::count<int>() << std::endl;

    std::cout << "is_permutation:" << std::endl;
    std::cout << List<int, float, char>::is_permutation<char, int, float>() << std::endl;
    std::cout << List<int, int, float, char>::is_permutation<float, int, char>() << std::endl;
    std::cout << List<int, float, char, char>::is_permutation<float, float, int, int, char>() << std::endl;
    std::cout << List<int, int, int, char, char>::is_permutation<int, char, int, char, int>() << std::endl;
    std::cout << List<>::is_permutation<int, int>() << std::endl;
    std::cout << List<char, int>::is_permutation<>() << std::endl;
    std::cout << List<>::is_permutation<>() << std::endl;

    return 0;
}
