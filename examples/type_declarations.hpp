#pragma once

// For examples and testing

#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/seq/for_each_i.hpp>
#include <boost/preprocessor/punctuation/comma_if.hpp>

// Type / Name
#define DECLARED_TYPES_SEQ \
((void, Void))\
((char, Char))\
((char unsigned, UnsignedChar))\
((int, Int))\
((int unsigned, UnsignedInt))\
((short, Short))\
((short unsigned, UnsignedShort))\
((long, Long))\
((long unsigned, UnsignedLong))\
((long long, LongLong))\
((long long unsigned, LongLongUnsigned))\
((float, Float))\
((double, Double))\

#define TYPE_EXP(R, MACRO, TYPE_DECL) MACRO TYPE_DECL
#define FOR_EACH_DECLARED_TYPE(MACRO) \
BOOST_PP_SEQ_FOR_EACH(TYPE_EXP, MACRO, DECLARED_TYPES_SEQ)

#define TYPE_LIST_EXP(R, MACRO, I, TYPE_DECL) BOOST_PP_COMMA_IF(I) MACRO TYPE_DECL
#define FOR_EACH_DECLARED_TYPE_LIST(MACRO) \
BOOST_PP_SEQ_FOR_EACH_I(TYPE_LIST_EXP, MACRO, DECLARED_TYPES_SEQ)

#define SELECT_TYPE(TYPE, NAME) TYPE
#define SELECT_NAME(TYPE, NAME) NAME

#define ALL_DECLARED_TYPES_LIST FOR_EACH_DECLARED_TYPE_LIST(SELECT_TYPE)

template<typename T> struct declared_name { static constexpr const char* value = "Unknown"; };
#define TYPE_NAME_MAPPING(TYPE, NAME) \
template<> struct declared_name<TYPE> { static constexpr const char* value = #NAME; };
FOR_EACH_DECLARED_TYPE(TYPE_NAME_MAPPING)
#undef TYPE_NAME_MAPPING
