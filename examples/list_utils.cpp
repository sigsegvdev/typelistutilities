#include "type_declarations.hpp"
#include <tlu/list_utils.hpp>
#include <iostream>

using namespace tlu;

template<typename ListT>
void printTypeListTypes() {
    auto printTypeName = [](auto holder) {
        using type = typename decltype(holder)::type;
        std::cout << declared_name<type>::value << ' ';
    };
    ListT::for_each(printTypeName);
    std::cout << std::endl;
}

template<typename ListT>
void printTypeListValues() {
    auto printTypeValue = [](auto holder) {
        using type = typename decltype(holder)::type;
        std::cout << type::value << ' ';
    };
    ListT::for_each(printTypeValue);
    std::cout << std::endl;
}

using AllTypesList = List<ALL_DECLARED_TYPES_LIST>;

template<typename T>
struct reverse {
    using type = AllTypesList::at<
        AllTypesList::size() - AllTypesList::index<T>() - 1
    >;
};

template<int I>
using Int = std::integral_constant<int, I>;

template<typename T>
struct is_even : std::bool_constant<T::value % 2 == 0> {};



int main(int, char**) {
    std::cout << "append:" << std::endl;
    printTypeListTypes<append<List<int, float, char>, long, double>::type>();
    printTypeListTypes<append<List<char, char, char>>::type>();
    printTypeListTypes<append<List<>, void, void, void>::type>();
    printTypeListTypes<append<List<>>::type>();

    std::cout << "append_distinct:" << std::endl;
    printTypeListTypes<append_distinct_i<List<int, float, char>, long, double>::type>();
    printTypeListTypes<append_distinct_i<List<int, char>, long, long, int, char, void>::type>();
    printTypeListTypes<append_distinct_i<List<int, int>, int, char>::type>();
    printTypeListTypes<append_distinct_i<List<int, int>, int, int, int>::type>();
    printTypeListTypes<append_distinct_i<List<int, char, float>>::type>();
    printTypeListTypes<append_distinct_i<List<>, int, float, char, float>::type>();
    printTypeListTypes<append_distinct_i<List<>>::type>();

    std::cout << "make_distinct" << std::endl;
    using DistinctfyList = List<int, char, float, float, char, int>;
    printTypeListTypes<make_distinct_i<DistinctfyList>::type>();
    printTypeListTypes<make_distinct_i<List<>>::type>();

    std::cout << "concatenate" << std::endl;
    printTypeListTypes<concatenate<
        List<int, char>, List<float, int>, List<void, char>
    >::type>();
    printTypeListTypes<concatenate<List<>, List<int, int>>::type>();
    printTypeListTypes<concatenate<List<>, List<>>::type>();
    printTypeListTypes<concatenate<>::type>();

    std::cout << "MergeTypeLists" << std::endl;
    printTypeListTypes<merge_i<
        List<int, char, int>,
        List<int, char, float, double>,
        List<float, int, char, void>
    >::type>();
    printTypeListTypes<merge_i<
        List<int, int>, List<>, List<char, char>
    >::type>();
    printTypeListTypes<merge_i<List<>, List<>, List<>>::type>();
    printTypeListTypes<merge_i<>::type>();

    std::cout << "transform" << std::endl;
    using TransformList = List<int, char, long, long long, int>;
    printTypeListTypes<transform<std::make_unsigned, TransformList>::type>();
    printTypeListTypes<transform<reverse, AllTypesList>::type>();
    printTypeListTypes<transform<reverse, List<>>::type>();

    std::cout << "FilterTypeLists" << std::endl;
    printTypeListTypes<filter<std::is_integral, AllTypesList>::type>();
    printTypeListValues<filter<is_even, 
        List<Int<1>, Int<2>, Int<3>, Int<4>, Int<5>, Int<6>, Int<7>>
    >::type>();
    printTypeListValues<filter<is_even, List<>>::type>();

    std::cout << "slice" << std::endl;
    printTypeListTypes<slice<AllTypesList, 3, 6>::type>();
    printTypeListTypes<slice<AllTypesList, 0, 5>::type>();
    printTypeListTypes<slice<AllTypesList, 0, (AllTypesList::size())>::type>();
    printTypeListTypes<slice<AllTypesList, (AllTypesList::size() - 4), (AllTypesList::size())>::type>();
    printTypeListTypes<slice<AllTypesList, 0, 0>::type>();
    printTypeListTypes<slice<List<>, 0, 0>::type>();

    return 0;    
}